/*
 * Copyright (c) 2007, Olof Naessen and Per Larsson
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright notice, 
 *      this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright notice, 
 *      this list of conditions and the following disclaimer in the documentation 
 *      and/or other materials provided with the distribution.
 *    * Neither the name of the Darkbits nor the names of its contributors may be 
 *      used to endorse or promote products derived from this software without 
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "blocktile.hpp"
#include "resourcehandler.hpp"
#include "levelhandler.hpp"

BlockTile::BlockTile(int x, int y)
: Tile(x, y)
{
    mDesertBlock = ResourceHandler::getInstance()->getBitmap("desert.bmp");
    mSewerBlock = ResourceHandler::getInstance()->getBitmap("bricks.bmp");
    mPrisonBlock = ResourceHandler::getInstance()->getBitmap("prisonblock.bmp");
}

BlockTile::~BlockTile()
{
}

void BlockTile::draw(BITMAP *dest, int frame, int scroll)
{
    int level = LevelHandler::getInstance()->getCurrentLevel();
    LevelModel* levelModel = LevelHandler::getInstance()->getLevel(level);

    if (levelModel->tileSet == 0)
    {	
        frame = frame % (mDesertBlock->w / 16);
	    masked_blit(mDesertBlock, dest, frame * 16, 0, getX() - scroll, getY(), 16, 16);
    }
    else if (levelModel->tileSet == 1)
    {
     	frame = frame % (mSewerBlock->w / 16);
        masked_blit(mSewerBlock, dest, frame * 16, 0, getX() - scroll, getY(), 16, 16);
    }
    else if (levelModel->tileSet == 2)
    {
     	frame = frame % (mPrisonBlock->w / 16);
        masked_blit(mPrisonBlock, dest, frame * 16, 0, getX() - scroll, getY(), 16, 16);
    }
}
