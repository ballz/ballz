/*
 * Copyright (c) 2007, Olof Naessen and Per Larsson
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright notice, 
 *      this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright notice, 
 *      this list of conditions and the following disclaimer in the documentation 
 *      and/or other materials provided with the distribution.
 *    * Neither the name of the Darkbits nor the names of its contributors may be 
 *      used to endorse or promote products derived from this software without 
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef BALLZ_TILEMAP_HPP
#define BALLZ_TILEMAP_HPP

#include <vector>
#include <iostream>
#include "tile.hpp"
#include "portaltile.hpp"
#include "doortile.hpp"
#include "doorbuttontile.hpp"

class TileMap
{
public:
    TileMap(unsigned int length);
    ~TileMap();
    void setTile(unsigned int type, int x, int y);
	Tile *getTileAtPixel(int x, int y);
	Tile *getTile(int x, int y);
    void draw(BITMAP *dest, int frame, int scroll);
    friend std::ostream &operator<<(std::ostream &os, TileMap &o);
    unsigned int getLength() { return mLength; }
    void logic();
    void connectDoorButtonsAndDoors();

protected:
	void updateFire();

    std::vector<Tile*> mMap;
    unsigned int mLength;
    PortalTile* mPortalP1;
    PortalTile* mPortalP2;
    PortalTile* mPortalQ1;
    PortalTile* mPortalQ2;
	BITMAP *mFire, *mFireTemp;
	int mFrame;
    std::vector<DoorTile*> mDoors;
    std::vector<DoorButtonTile*> mDoorButtons;
};

#endif
