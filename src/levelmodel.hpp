/*
 * Copyright (c) 2007, Olof Naessen and Per Larsson
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright notice, 
 *      this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright notice, 
 *      this list of conditions and the following disclaimer in the documentation 
 *      and/or other materials provided with the distribution.
 *    * Neither the name of the Darkbits nor the names of its contributors may be 
 *      used to endorse or promote products derived from this software without 
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef BALLZ_LEVELMODEL_HPP
#define BALLZ_LEVELMODEL_HPP

class LevelModel
{
public:
    LevelModel()
        :numberOfStars(0),
         numberOfCollectedStars(0),
         bestTime(0),
         starsCollectedUntilUnlocked(0),
         tileSet(0)
    {}
    LevelModel(const std::string& name_,
               const std::string& mapfile_,
               int numberOfStars_,
               int starsCollectedUntilUnlocked_,
               int tileSet_)
               :name(name_),
               mapfile(mapfile_),
               numberOfStars(numberOfStars_),
               numberOfCollectedStars(0),
               bestTime(-1),
               starsCollectedUntilUnlocked(starsCollectedUntilUnlocked_),
               tileSet(tileSet_)
    {}
    std::string name;
    std::string mapfile;
    int numberOfStars;
    int numberOfCollectedStars;
    int bestTime;
    int starsCollectedUntilUnlocked;
    int tileSet;
};

#endif
