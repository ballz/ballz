/*
 * Copyright (c) 2007, Olof Naessen and Per Larsson
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright notice, 
 *      this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright notice, 
 *      this list of conditions and the following disclaimer in the documentation 
 *      and/or other materials provided with the distribution.
 *    * Neither the name of the Darkbits nor the names of its contributors may be 
 *      used to endorse or promote products derived from this software without 
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef BALLZ_GAME_HPP
#define BALLZ_GAME_HPP

#include "level.hpp"
#include "gui/ballzlistbox.hpp"
#include "gui/levelselector.hpp"
#include "guichan.hpp"
#include "guichan/allegro.hpp"
#include "gui/ballzscrollarea.hpp"

class Game: public gcn::ActionListener, public gcn::KeyListener
{
public:
	Game();

	~Game();
	
	void logic();

	void draw(BITMAP *buffer);

	void run();

	void quit();

    void action(const gcn::ActionEvent& actionEvent);

    void keyPressed(gcn::KeyEvent &keyEvent);

	void requestFade();

private:

    void initGui();

    void drawSplashScreen(BITMAP* dest);

    class MainMenuListModel: public gcn::ListModel
    {
    public:
        MainMenuListModel(Game* game) : ListModel(), game(game) {}
        int getNumberOfElements()
        {
            return 3;
        }

        std::string getElementAt(int i)
        {
            switch(i)
            {
            case 0:
                return std::string("PLAY");
            case 1:
                if (game->fullscreen)
		  return std::string("WINDOWED");
                else  
                  return std::string("FULLSCREEN");
            case 2:
                return std::string("CREDITS");
            default: // Just to keep warnings away
              return std::string("");
            }
        }
    private:
      Game* game;
    };


	Level *level;
	
	enum State {
		MENU,
		LEVEL,
		EXIT
	};

	bool fullscreen;
	bool fadeRequested;
	State state;
    gcn::Gui* mGui;
    gcn::AllegroGraphics* mAllegroGraphics;
    gcn::AllegroImageLoader* mAllegroImageLoader;
    gcn::AllegroInput* mAllegroInput;
    gcn::ImageFont* mImageFont;

    gcn::Container* mTop;

    gcn::Container* mMainMenuContainer;
    BallzListBox* mMainMenuListBox;
    MainMenuListModel* mMainMenuListModel;

    gcn::Container* mCreditsContainer;
    gcn::Image* mOlofImage;
    gcn::Icon* mOlofIcon;
    gcn::Label* mOlofLabel;
    gcn::Image* mPerImage;
    gcn::Icon* mPerIcon;
    gcn::Label* mPerLabel;

    gcn::Container* mLevelsContainer;
    LevelSelector* mLevelSelector;
    BallzScrollArea* mLevelSelectorScrollArea;

    gcn::TextBox* mInfoText;
    gcn::ImageFont* mInfoTextFont;

    gcn::Image* mTopBackgroundImage;
    gcn::Icon* mTopBackgroundIcon;
    gcn::Image* mBallzLogoImage;
    gcn::Icon* mBallzLogoIcon;
    gcn::Label* mCollectedStars;

    gcn::TextBox* mCreditsText;

};

#endif
